Function Get-SSLThumbprint {
	param(
		[Parameter(Position=0,Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)][Alias('FullName')][String]$URL
	)
add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class IDontCarePolicy : ICertificatePolicy {
public IDontCarePolicy() {}
public bool CheckValidationResult(
ServicePoint sPoint, X509Certificate cert,
WebRequest wRequest, int certProb) {
return true;
}
}
"@
[System.Net.ServicePointManager]::CertificatePolicy = new-object IDontCarePolicy

# Need to connect using simple GET operation for this to work
Invoke-RestMethod -Uri $URL -Method Get | Out-Null

$ENDPOINT_REQUEST = [System.Net.Webrequest]::Create("$URL")
$SSL_THUMBPRINT = $ENDPOINT_REQUEST.ServicePoint.Certificate.GetCertHashString()

return $SSL_THUMBPRINT -replace '(..(?!$))','$1:'
}

Function Open-VMConsole {
	[CmdletBinding()]
	Param (
		[Parameter(Mandatory=$true,  ValueFromPipeline=$true)][ValidateNotNullOrEmpty()][string]$VM, 
		[Parameter(Mandatory=$False)][PSCredential]$PsCreds,
		[Parameter(Mandatory=$False)][string]$VMRCBin="C:\Program Files (x86)\VMware\VMware Remote Console\vmrc.exe",
		[parameter(mandatory = $false, ParameterSetName="H5")][Switch]$HTML5
	)
	Begin{

	}
	Process{
		If($myvm = Get-VM $vm -ea SilentlyContinue){
			$VCInstance = ($global:DefaultVIServers | Where {$_.Name -eq (($myvm.uid.Split("@")[1] -split(":"))[0])})
			If($HTML5){
				$VCSSLThumbprint = Get-SSLThumbprint "https://$($VCInstance.name)" -ea silentlycontinue
				$SessionMgr = Get-View $VCInstance.ExtensionData.Content.SessionManager
				$Ticket = $SessionMgr.AcquireCloneTicket()
				$URL = "https://" + ($myvm.uid.Split("@")[1] -split(":"))[0] + "/ui/webconsole.html?vmId=" + $myvm.ExtensionData.MoRef.Value + "&vmName=" + $myvm.name + "&serverGuid=" + $VCInstance.InstanceUuid + "&locale=en_US&host=" + $VCInstance.name + ":443&sessionTicket=" + $Ticket + "&thumbprint=" + $VCSSLThumbprint
			}
			Else{
				If($PsCreds){
					$URL = "vmrc://" + $PsCreds.username + "@" + ($myvm.uid.Split("@")[1] -split(":"))[0] + ":443/?moid=" + $myvm.ExtensionData.MoRef.Value
				}
				Else{
					$URL = "vmrc://"+ ($myvm.uid.Split("@")[1] -split(":"))[0] + ":443/?moid=" + $myvm.ExtensionData.MoRef.Value
				}
			
			}
			Start-Process -FilePath $URL
		}
		Else{
			Write-Host "Unable to find VM $($vm)" -ForegroundColor Red
		}
	}
	End{
	}
}